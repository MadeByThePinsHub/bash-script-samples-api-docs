---
title: Installation
description: How to install the API wrapper, server and CLI
tags: setup
---

> :warning: Before going any further, make sure you meet all the [requirements](/install/requirements).
{.is-info}

## By Type
* [API server](/install/server)
* [API wrapper *coming soon*](/install/api-wrapper)
* [CLI](/install/cli)
{.links-list}