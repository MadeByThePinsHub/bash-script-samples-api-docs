---
title: Homepage
description: 
published: true
date: 2020-05-08T04:54:43.390Z
tags: community hub, docs
---

> :warning: Documentation is being worked on! Some pages may be incomplete or not exist yet. {.is-warning}

# Getting Started
To access the API endpoints without using `curl`, our API wrappers and CLI are quick and to install
* [Requirements *Before you get started, make sure you have these.*](/install/requirements)
* [Installing the API server *Even our API server is under development, proceed with caution.*](/install/server)
* [Installing the CLI *The CLI is under development, proceed with caution.*](/install/cli)
* [Installing the API Wrappers *Soon, stop yeeting.*](/install/api-wrappers)
{.links-list}

# User Guide
* [The Basics *Learn the basics of using the API endpoints*](/guide/intro)
* [Authentication *Soon, stop yeeting.*](/guide/auth)
* [The GraphiQL Endpoint *Work in progress.*](/guide/graph-api)
{.links-list}

# Customization
* [Configuration *You are free as bird to configure CLI and API wrappers*](install/config)
{.links-list}

# Developers
Our API server, the wrappers and the CLI itself are open-source and fully extensible for maximum customization.

* [:book: Getting Started](dev)
{.links-list}